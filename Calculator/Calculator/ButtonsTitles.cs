﻿namespace Calculator
{
    public class ButtonsTitles
    {
        public const string Cancel = "C";
        public const string Zero = "0";
        public const string One = "1";
        public const string Two = "2";
        public const string Three = "3";
        public const string Four = "4";
        public const string Five = "5";
        public const string Six = "6";
        public const string Seven = "7";
        public const string Eight = "8";
        public const string Nine = "9";
        public const string Point = ".";
        public const string PlusMinus = "+/-";
        public const string Percents = "%";
        public const string Plus = "+";
        public const string Minus = "-";
        public const string Mult = "X";
        public const string Div = "/";
        public const string Equal = "=";
    }
}
