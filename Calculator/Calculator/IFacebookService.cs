﻿using System.Threading.Tasks;

namespace Calculator
{
    public interface IFacebookService
    {
        Task<bool> Login ();
        Task<bool> Post (string text);
    }
}
