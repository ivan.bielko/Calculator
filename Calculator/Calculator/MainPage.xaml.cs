﻿using System;
using Xamarin.Forms;

namespace Calculator
{
    public partial class MainPage : ContentPage
    {
        const string Error = "ERROR";

        Operations _operation = Operations.None;
        bool _isNumberChanging;
        float _firstNumer = 0;
        float _secondNumer = 0;

        public MainPage ()
        {
            InitializeComponent ();
        }

        public async void FacebookClicked (object sender, EventArgs e)
        {
            var facebookService = (Application.Current as App)?.FacebookService;
            if(facebookService!=null && await facebookService.Login () && await facebookService.Post (displayLabel.Text))
            {
                await DisplayAlert (string.Empty, "Posted", "OK");
            }else
            {
                await DisplayAlert (string.Empty, "Error", "OK");
            }
        }

        public void ButtonsClicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            DefineOperation (button?.Text);
        }

        void DefineOperation(string buttonTitle)
        {
            switch(buttonTitle)
            {
                case ButtonsTitles.Zero:
                case ButtonsTitles.One:
                case ButtonsTitles.Two:
                case ButtonsTitles.Three:
                case ButtonsTitles.Four:
                case ButtonsTitles.Five:
                case ButtonsTitles.Six:
                case ButtonsTitles.Seven:
                case ButtonsTitles.Eight:
                case ButtonsTitles.Nine:
                    displayLabel.Text = ProccedDigit (buttonTitle, displayLabel.Text);
                    break;
                case ButtonsTitles.Cancel:
                    displayLabel.Text = DoCancel ();
                    break;
                case ButtonsTitles.Div:
                    displayLabel.Text = ProccedEqual (displayLabel.Text);
                    _operation = Operations.Div;
                    ProccedOperation (displayLabel.Text);
                    break;
                case ButtonsTitles.Mult:
                    displayLabel.Text = ProccedEqual (displayLabel.Text);
                    _operation = Operations.Mult;
                    ProccedOperation (displayLabel.Text);
                    break;
                case ButtonsTitles.Plus:
                    displayLabel.Text = ProccedEqual (displayLabel.Text);
                    _operation = Operations.Plus;
                    ProccedOperation (displayLabel.Text);
                    break;
                case ButtonsTitles.Minus:
                    displayLabel.Text = ProccedEqual (displayLabel.Text);
                    _operation = Operations.Minus;
                    ProccedOperation (displayLabel.Text);
                    break;
                case ButtonsTitles.PlusMinus:
                    displayLabel.Text = ProccedPlusMinus (displayLabel.Text);
                    break;
                case ButtonsTitles.Equal:
                    displayLabel.Text = ProccedEqual (displayLabel.Text);
                    break;
                case ButtonsTitles.Percents:
                    displayLabel.Text = ProccedPercents (displayLabel.Text);
                    break;
                case ButtonsTitles.Point:
                    displayLabel.Text = ProccedPoint (displayLabel.Text);
                    break;
                default:
                    displayLabel.Text = Error;
                    break;

            }
        }

        private string ProccedPoint (string text)
        {
            if(_isNumberChanging)
            {
                _isNumberChanging = false;
                return $"{ButtonsTitles.Zero}{ButtonsTitles.Point}";
            }
            return $"{text}{ButtonsTitles.Point}";
        }

        private string ProccedPercents (string text)
        {
            if (float.TryParse (text, out _secondNumer))
            {
                _secondNumer = _firstNumer / 100 * _secondNumer;
                var result = DoCalculate (_firstNumer, _secondNumer);
                _isNumberChanging = true;
                _operation = Operations.None;
                if (result != float.NaN)
                    return result.ToString ();
                return Error;
            }
            return Error;
        }

        private string ProccedEqual (string text)
        {
            if(float.TryParse(text, out _secondNumer))
            {
                var result = DoCalculate (_firstNumer, _secondNumer);
                _isNumberChanging = true;
                _operation = Operations.None;
                if (result != float.NaN)
                    return result.ToString ();
                return Error;
            }
            return Error;
        }

        private void ProccedOperation (string text)
        {
            if(float.TryParse(text, out _firstNumer))
            {
                _isNumberChanging = true;
            }
        }

        private string ProccedPlusMinus (string text)
        {
            if (_isNumberChanging)
            {
                _isNumberChanging = false;
                return $"{ButtonsTitles.Minus}{ButtonsTitles.Zero}";
            }
            if(text.StartsWith(ButtonsTitles.Minus))
            {
                return text.Replace (ButtonsTitles.Minus, string.Empty);
            }else
            {
                return $"{ButtonsTitles.Minus}{text}";
            }
        }

        private string DoCancel ()
        {
            _firstNumer = 0;
            _secondNumer = 0;
            _operation = Operations.None;
            _isNumberChanging = false;
            return ButtonsTitles.Zero;
        }

        private string ProccedDigit (string buttonTitle, string displayText)
        {
            if (displayText.Equals (ButtonsTitles.Zero) || _isNumberChanging)
            {
                _isNumberChanging = false;
                return buttonTitle;
            }
            if (displayText.Equals ($"{ButtonsTitles.Minus}{ButtonsTitles.Zero}"))
                return $"{ButtonsTitles.Minus}{buttonTitle}";
            return $"{displayText}{buttonTitle}";
        }

        private float DoCalculate(float firstValue, float secondValue)
        {
            switch(_operation)
            {
                case Operations.Div:
                    return firstValue / secondValue;
                case Operations.Minus:
                    return firstValue - secondValue;
                case Operations.Mult:
                    return firstValue * secondValue;
                case Operations.Plus:
                    return firstValue + secondValue;
                case Operations.None:
                    return secondValue;
                default:
                    return float.NaN;
            }
        }
    }
}
