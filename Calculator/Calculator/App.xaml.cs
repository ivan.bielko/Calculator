﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Calculator
{
    public partial class App : Application
    {
        public IFacebookService FacebookService { get; set; }
        public IPictureService PictureService { get; set; }

        public App ()
        {
            InitializeComponent ();

            MainPage = new Calculator.MainPage ();
        }

        protected override void OnStart ()
        {
            // Handle when your app starts
        }

        protected override void OnSleep ()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume ()
        {
            // Handle when your app resumes
        }
    }
}
