using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Social;
using Xamarin.Social.Services;

namespace Calculator.Droid
{
    public class AndroidFacebookService : IFacebookService
    {
        readonly FacebookService _facebook;

        public AndroidFacebookService ()
        {
            _facebook = new FacebookService { ClientId = Constants.FacebookAppId, Scope = "publish_actions", RedirectUrl = new System.Uri ("http://calculator.com") };
        }

        public Task<bool> Login ()
        {
            var t = new TaskCompletionSource<bool> ();

            var context = Forms.Context as Context;
            if (context == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var activity = Forms.Context as Activity;
            if (activity == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var loginIntent = _facebook.GetAuthenticateUI (activity, account =>
             {
                 _facebook.SaveAccount (Android.App.Application.Context, account);
                 t.TrySetResult (!string.IsNullOrWhiteSpace (account?.Properties["access_token"]));
             });
            context.StartActivity (loginIntent);

            return t.Task;
        }

        public Task<bool> Post (string text)
        {
            var item = new Item { Text = text };

            var t = new TaskCompletionSource<bool> ();

            var context = Forms.Context as Context;
            if (context == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var activity = Forms.Context as Activity;
            if (activity == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var shareIntent = _facebook.GetShareUI (activity, item, result =>
            {
                t.TrySetResult (result == ShareResult.Done);
            });
            context.StartActivity (shareIntent);

            return t.Task;
        }
    }
}