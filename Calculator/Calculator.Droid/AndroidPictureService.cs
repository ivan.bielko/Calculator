using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

namespace Calculator.Droid
{
    public class AndroidPictureService : IPictureService
    {
        TaskCompletionSource<string> _tcs;

        public Task<string> ChoosePicture ()
        {
            _tcs = new TaskCompletionSource<string> ();

            var context = Forms.Context as Activity;
            if (context == null)
            {
                _tcs.TrySetResult (null);
                return _tcs.Task;
            }

            var imageIntent = new Intent ();
            imageIntent.SetType ("image/*");
            imageIntent.SetAction (Intent.ActionGetContent);

            context.StartActivityForResult (
                Intent.CreateChooser (imageIntent, "Select photo"), 0);

            return _tcs.Task;
        }

        public void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                _tcs.TrySetResult (data.DataString);
            }
        }
    }
}