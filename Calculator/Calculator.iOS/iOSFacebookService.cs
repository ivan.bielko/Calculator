﻿using System.Threading.Tasks;
using UIKit;
using Xamarin.Social;
using Xamarin.Social.Services;

namespace Calculator.iOS
{
    public class iOSFacebookService : IFacebookService
    {
        readonly FacebookService _facebook;
        public iOSFacebookService ()
        {
            _facebook = new FacebookService { ClientId = Constants.FacebookAppId, Scope = "publish_actions", RedirectUrl = new System.Uri ("http://calculator.com") };
        }

        public Task<bool> Login ()
        {
            var t = new TaskCompletionSource<bool> ();

            var navigationController = ((AppDelegate)(UIApplication.SharedApplication.Delegate))?.Window?.RootViewController?.NavigationController;
            if (navigationController == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var loginView = _facebook.GetAuthenticateUI (account =>
             {
                 _facebook.SaveAccount (account);
                 t.TrySetResult (true);
             });
            navigationController.PushViewController (loginView, true);

            return t.Task;
        }

        public Task<bool> Post (string text)
        {
            var item = new Item { Text = text };

            var t = new TaskCompletionSource<bool> ();

            var navigationController = ((AppDelegate)(UIApplication.SharedApplication.Delegate))?.Window?.RootViewController?.NavigationController;
            if (navigationController == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var shareView = _facebook.GetShareUI (item, result =>
            {
                t.TrySetResult (result == ShareResult.Done);
            });
            navigationController.PushViewController (shareView, true);

            return t.Task;
        }
    }
}
