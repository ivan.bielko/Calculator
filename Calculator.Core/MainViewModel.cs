﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculator.Core
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IFacebookService _facebookService;
        private readonly IDialogService _dialogService;
        private readonly IPictureService _pictureService;
        private readonly Calculator _calculator;

        string _displayText;

        public string DisplayText
        {
            get
            {
                return _displayText;
            }
            set
            {
                _displayText = value;
                NotifyPropertyChanged ("DisplayText");
            }
        }

        public ICommand ZeroCommand => new Command (()=>DoDigit("0"));
        public ICommand OneCommand => new Command (() => DoDigit ("1"));
        public ICommand TwoCommand => new Command (() => DoDigit ("2"));
        public ICommand ThreeCommand => new Command (() => DoDigit ("3"));
        public ICommand FourCommand => new Command (() => DoDigit ("4"));
        public ICommand FiveCommand => new Command (() => DoDigit ("5"));
        public ICommand SixCommand => new Command (() => DoDigit ("6"));
        public ICommand SevenCommand => new Command (() => DoDigit ("7"));
        public ICommand EightCommand => new Command (() => DoDigit ("8"));
        public ICommand NineCommand => new Command (() => DoDigit ("9"));
        public ICommand CancelCommand => new Command (DoCancel);
        public ICommand PlusMinusCommand => new Command (DoPlusMinus);
        public ICommand PercentsCommand => new Command (DoPercents);
        public ICommand DivCommand => new Command (DoDiv);
        public ICommand MultCommand => new Command (DoMult);
        public ICommand MinusCommand => new Command (DoMinus);
        public ICommand PlusCommand => new Command (DoPlus);
        public ICommand EqualCommand => new Command (DoEqual);
        public ICommand PointCommand => new Command (DoPoint);
        public ICommand FacebookLoginCommand => new Command (async ()=> await DoFacebookLogin());

        public MainViewModel (IFacebookService facebookService,
            IDialogService dialogService, IPictureService pictureService)
        {
            _facebookService = facebookService;
            _dialogService = dialogService;
            _pictureService = pictureService;
            _calculator = new Calculator ();
        }

        public override void Init ()
        {
            DisplayText = "0";
        }

        void DoDigit (string digit)
        {
            DisplayText = _calculator.ProccedDigit (digit, DisplayText);
        }

        void DoCancel ()
        {
            DisplayText = _calculator.DoCancel ();
        }

        void DoPlusMinus ()
        {
            DisplayText = _calculator.ProccedPlusMinus (DisplayText);
        }

        void DoPercents ()
        {
            DisplayText = _calculator.ProccedPercents (DisplayText);
        }

        void DoDiv ()
        {
            DisplayText = _calculator.ProccedOperation (DisplayText, Operations.Div);
        }

        void DoMult ()
        {
            DisplayText = _calculator.ProccedOperation (DisplayText, Operations.Mult);
        }

        void DoPlus ()
        {
            DisplayText = _calculator.ProccedOperation (DisplayText, Operations.Plus);
        }

        void DoMinus ()
        {
            DisplayText = _calculator.ProccedOperation (DisplayText, Operations.Minus);
        }

        void DoEqual ()
        {
            DisplayText = _calculator.ProccedEqual (DisplayText);
        }

        void DoPoint ()
        {
            DisplayText = _calculator.ProccedPoint (DisplayText);
        }

        async Task DoFacebookLogin ()
        {
            var logged = await _facebookService.Login ();
            if (logged)
            {
                var addPicture = await _dialogService.ShowDialogWithResult ("Facebook post",
                    "Add image to post?", "Yes", "No");
                object pictureData = null;
                if (addPicture)
                {
                    pictureData = await _pictureService.ChoosePicture ();
                }
                var posted = await _facebookService.Post (DisplayText, pictureData);
                if (posted)
                    _dialogService.ShowToast ("Posted");
                else
                    _dialogService.ShowToast ("Not posted");
            }
            else
            {
                _dialogService.ShowToast ("Not logged in");
            }
        }
    }
}
