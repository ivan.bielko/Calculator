﻿using System;
using System.Threading.Tasks;

namespace Calculator.Core
{
    public interface IDialogService
    {
        Task<bool> ShowDialogWithResult (string title, string message, string positiveTitle, string negativeTitle);
        void ShowToast (string message, bool isShort = true);
    }
}
