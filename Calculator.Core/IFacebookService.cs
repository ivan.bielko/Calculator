﻿using System.Threading.Tasks;

namespace Calculator.Core
{
    public interface IFacebookService
    {
        Task<bool> Login ();
        Task<bool> Post (string text, object imageUri = null);
    }
}
