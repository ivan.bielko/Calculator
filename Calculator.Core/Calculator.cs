﻿namespace Calculator.Core
{
    public class Calculator
    {
        const string Error = "ERROR";
        const string Zero = "0";
        const string Point = ".";
        const string Minus = "-";

        const int MaxDisplayLength = 16;

        Operations _operation = Operations.None;
        bool _isNumberChanging;
        float _firstNumer = 0;
        float _secondNumer = 0;

        public string ProccedPoint (string text)
        {
            if (_isNumberChanging)
            {
                _isNumberChanging = false;
                return $"{Zero}{Point}";
            }
            return $"{text}{Point}";
        }

        public string ProccedPercents (string text)
        {
            if (float.TryParse (text, out _secondNumer))
            {
                _secondNumer = _firstNumer / 100 * _secondNumer;
                var result = DoCalculate (_firstNumer, _secondNumer);
                _isNumberChanging = true;
                _operation = Operations.None;
                if (result != float.NaN)
                    return result.ToString ();
                return Error;
            }
            return Error;
        }

        public string ProccedEqual (string text)
        {
            if (float.TryParse (text, out _secondNumer))
            {
                var result = DoCalculate (_firstNumer, _secondNumer);
                _isNumberChanging = true;
                _operation = Operations.None;
                if (result != float.NaN)
                    return result.ToString ();
                return Error;
            }
            return Error;
        }

        public string ProccedOperation (string text, Operations operation)
        {
            var equalResult = ProccedEqual (text);
            _operation = operation;
            if (float.TryParse (equalResult, out _firstNumer))
            {
                _isNumberChanging = true;
            }

            return equalResult;
        }

        public string ProccedPlusMinus (string text)
        {
            if (_isNumberChanging)
            {
                _isNumberChanging = false;
                return $"{Minus}{Zero}";
            }
            if (text.StartsWith (Minus))
            {
                return text.Replace (Minus, string.Empty);
            }
            else
            {
                return $"{Minus}{text}";
            }
        }

        public string DoCancel ()
        {
            _firstNumer = 0;
            _secondNumer = 0;
            _operation = Operations.None;
            _isNumberChanging = false;
            return Zero;
        }

        public string ProccedDigit (string buttonTitle, string displayText)
        {
            if (string.IsNullOrWhiteSpace (displayText) || string.IsNullOrWhiteSpace (buttonTitle))
                return displayText;

            if (displayText.Equals (Zero) || _isNumberChanging)
            {
                _isNumberChanging = false;
                return buttonTitle;
            }
            if (displayText.Equals ($"{Minus}{Zero}"))
                return $"{Minus}{buttonTitle}";
            return $"{displayText}{buttonTitle}";
        }

        public float DoCalculate (float firstValue, float secondValue)
        {
            switch (_operation)
            {
                case Operations.Div:
                    return firstValue / secondValue;
                case Operations.Minus:
                    return firstValue - secondValue;
                case Operations.Mult:
                    return firstValue * secondValue;
                case Operations.Plus:
                    return firstValue + secondValue;
                case Operations.None:
                    return secondValue;
                default:
                    return float.NaN;
            }
        }
    }
}
