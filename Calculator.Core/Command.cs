﻿using System;
using System.Windows.Input;

namespace Calculator.Core
{
    public class Command : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public Action _execute;

        public Command(Action execute)
        {
            _execute = execute;
        }

        public bool CanExecute (object parameter)
        {
            return _execute != null;
        }

        public void Execute (object parameter)
        {
            _execute ();
        }
    }
}
