using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator.Core
{
    public class IntentDataEventArgs : EventArgs
    {
        public object IntentData { get; }
        public bool IsOK { get; }
        public int RequestCode { get; }

        public IntentDataEventArgs(object intentData, bool isOK, int requestCode)
        {
            this.IntentData = intentData;
            this.IsOK = isOK;
            this.RequestCode = requestCode;
        }
    }
}