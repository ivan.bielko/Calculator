using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Calculator.Core;

namespace Calculator.Android
{
    public abstract class BaseActivity : Activity
    {
        bool _initialized;

        BaseViewModel _viewModel;
        public BaseViewModel DataSource {
            get
            {
                return _viewModel;
            }
            set
            {
                _viewModel = value;
                _viewModel.PropertyChanged += DataSource_PropertyChanged;
            }
        }

        public event EventHandler<IntentDataEventArgs> OnActivityResultRecieved;

        protected override void OnStart ()
        {
            base.OnStart ();
            if (!_initialized)
            {
                _viewModel.Init ();
                _initialized = true;
            }
        }

        readonly Dictionary<string, object> _bindableObjects = new Dictionary<string, object> ();
        readonly Dictionary<string, string> _bindableProperties = new Dictionary<string, string> ();

        private void DataSource_PropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(_bindableObjects.Keys.Contains(e.PropertyName))
            {
                var obj = _bindableObjects[e.PropertyName];
                var propName = _bindableProperties[e.PropertyName];
                var newValue = sender.GetType ().GetProperty (e.PropertyName).GetValue (sender);
                obj.GetType ().GetProperty (propName).SetValue(obj, newValue);
            }
        }

        protected void Bind(object obj, string property, string viewModelProperty)
        {
            _bindableObjects.Add (viewModelProperty, obj);
            _bindableProperties.Add (viewModelProperty, property);
        }

        protected void Bind (Button button, ICommand command)
        {
            button.Click += delegate
              {
                  command.Execute (new object());
              };
        }

        protected override void OnActivityResult (int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult (requestCode, resultCode, data);
            OnActivityResultRecieved (this, new IntentDataEventArgs (data, resultCode == Result.Ok, requestCode));
        }
    }
}