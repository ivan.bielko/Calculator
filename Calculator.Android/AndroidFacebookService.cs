using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using Android.Provider;
using Calculator.Core;
using Xamarin.Social;
using Xamarin.Social.Services;

namespace Calculator.Core
{
    public class AndroidFacebookService : IFacebookService
    {
        readonly FacebookService _facebook;
        readonly Context _context;

        public AndroidFacebookService (Context context)
        {
            _context = context;
            _facebook = new FacebookService { ClientId = Constants.FacebookAppId, Scope = "publish_actions", RedirectUrl = new System.Uri ("http://calculator.com") };
        }

        public Task<bool> Login ()
        {
            var t = new TaskCompletionSource<bool> ();

            var activity = _context as Activity;
            if (activity == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var loginIntent = _facebook.GetAuthenticateUI (activity, account =>
             {
                 if (account != null)
                     _facebook.SaveAccount (_context, account);
                 t.TrySetResult (!string.IsNullOrWhiteSpace (account?.Properties["access_token"]));
             });
            _context.StartActivity (loginIntent);

            return t.Task;
        }

        public Task<bool> Post (string text, object imageUri = null)
        {
            var t = new TaskCompletionSource<bool> ();

            var item = new Item { Text = text };
            if (imageUri != null)
            {
                var uri = imageUri as Uri;
                var bitmap = MediaStore.Images.Media.GetBitmap (_context.ContentResolver, uri);
                var imageData = new ImageData (bitmap);
                item.Images.Add (imageData);
            }

            var activity = _context as Activity;
            if (activity == null)
            {
                t.TrySetResult (false);
                return t.Task;
            }

            var shareIntent = _facebook.GetShareUI (activity, item, result =>
            {
                t.TrySetResult (result == ShareResult.Done);
            });
            _context.StartActivity (shareIntent);

            return t.Task;
        }
    }
}