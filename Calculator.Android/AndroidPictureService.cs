using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Calculator.Core;

namespace Calculator.Android
{
    public class AndroidPictureService : IPictureService
    {
        const int requestCode = 42;
        TaskCompletionSource<object> _tcs;
        readonly Context _context;

        public AndroidPictureService(Context context)
        {
            _context = context;
            (_context as BaseActivity).OnActivityResultRecieved += AndroidPictureService_OnActivityResultRecieved;
        }

        private void AndroidPictureService_OnActivityResultRecieved (object sender, IntentDataEventArgs e)
        {
            if (e.RequestCode == requestCode)
            {
                if(e.IsOK)
                {
                    _tcs.TrySetResult ((e.IntentData as Intent)?.Data);
                }else
                {
                    _tcs.TrySetResult (null);
                }
            }
        }

        public Task<object> ChoosePicture ()
        {
            _tcs = new TaskCompletionSource<object> ();

            var activity = _context as Activity;
            if (activity == null)
            {
                _tcs.TrySetResult (null);
                return _tcs.Task;
            }

            var imageIntent = new Intent ();
            imageIntent.SetType ("image/*");
            imageIntent.SetAction (Intent.ActionGetContent);
            activity.StartActivityForResult (
                Intent.CreateChooser (imageIntent, "Select photo"), requestCode);

            return _tcs.Task;
        }
    }
}