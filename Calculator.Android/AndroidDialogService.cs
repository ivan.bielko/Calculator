using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Calculator.Core;

namespace Calculator.Android
{
    public class AndroidDialogService : IDialogService
    {
        readonly Context _context;
        public AndroidDialogService(Context context)
        {
            _context = context;
        }

        public Task<bool> ShowDialogWithResult (string title, string message,
            string positiveTitle, string negativeTitle)
        {
            var t = new TaskCompletionSource<bool> ();

            new AlertDialog.Builder (_context)
                .SetPositiveButton (positiveTitle, (sender, args) =>
                {
                    t.TrySetResult (true);
                })
                .SetNegativeButton (negativeTitle, (sender, args) =>
                {
                    t.TrySetResult (false);
                })
                .SetMessage (message)
                .SetTitle (title)
                .Show ();

            return t.Task;
        }

        public void ShowToast (string message, bool isShort = true)
        {
            Toast.MakeText (_context, message, isShort ? ToastLength.Short : ToastLength.Long);
        }
    }
}