﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Java.Interop;
using Xamarin.Social;
using Xamarin.Social.Services;
using Calculator.Core;

namespace Calculator.Android
{
    [Activity (Label = "Calculator", ScreenOrientation = ScreenOrientation.FullUser, MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : BaseActivity
    {
        MainViewModel _viewModel;

        TextView _displayTextView;
        Button _zeroButton;
        Button _oneButton;
        Button _twoButton;
        Button _threeButton;
        Button _fourButton;
        Button _fiveButton;
        Button _sixButton;
        Button _sevenButton;
        Button _eightButton;
        Button _nineButton;
        Button _cancelButton;
        Button _plusminusButton;
        Button _percentsButton;
        Button _divButton;
        Button _multButton;
        Button _plusButton;
        Button _minusButton;
        Button _equalButton;
        Button _pointButton;
        Button _facebookButton;

        public MainActivity ()
        {
            _viewModel = new MainViewModel (new AndroidFacebookService (this),
                new AndroidDialogService(this),
                new AndroidPictureService(this));
            DataSource = _viewModel;
        }

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            _displayTextView = FindViewById<TextView> (Resource.Id.displayTextView);
            _zeroButton = FindViewById<Button> (Resource.Id.zeroButton);
            _oneButton = FindViewById<Button> (Resource.Id.oneButton);
            _twoButton = FindViewById<Button> (Resource.Id.twoButton);
            _threeButton = FindViewById<Button> (Resource.Id.threeButton);
            _fourButton = FindViewById<Button> (Resource.Id.fourButton);
            _fiveButton = FindViewById<Button> (Resource.Id.fiveButton);
            _sixButton = FindViewById<Button> (Resource.Id.sixButton);
            _sevenButton = FindViewById<Button> (Resource.Id.sevenButton);
            _eightButton = FindViewById<Button> (Resource.Id.eightButton);
            _nineButton = FindViewById<Button> (Resource.Id.nineButton);
            _cancelButton = FindViewById<Button> (Resource.Id.cancelButton);
            _plusminusButton = FindViewById<Button> (Resource.Id.plusminusButton);
            _percentsButton = FindViewById<Button> (Resource.Id.percentsButton);
            _divButton = FindViewById<Button> (Resource.Id.divButton);
            _multButton = FindViewById<Button> (Resource.Id.multButton);
            _plusButton = FindViewById<Button> (Resource.Id.plusButton);
            _minusButton = FindViewById<Button> (Resource.Id.minusButton);
            _equalButton = FindViewById<Button> (Resource.Id.equalButton);
            _pointButton = FindViewById<Button> (Resource.Id.pointButton);
            _facebookButton = FindViewById<Button> (Resource.Id.facebookButton);

            Bind (_displayTextView, "Text", "DisplayText");
            Bind (_zeroButton, _viewModel.ZeroCommand);
            Bind (_oneButton, _viewModel.OneCommand);
            Bind (_twoButton, _viewModel.TwoCommand);
            Bind (_threeButton, _viewModel.ThreeCommand);
            Bind (_fourButton, _viewModel.FourCommand);
            Bind (_fiveButton, _viewModel.FiveCommand);
            Bind (_sixButton, _viewModel.SixCommand);
            Bind (_sevenButton, _viewModel.SevenCommand);
            Bind (_eightButton, _viewModel.EightCommand);
            Bind (_nineButton, _viewModel.NineCommand);
            Bind (_cancelButton, _viewModel.CancelCommand);
            Bind (_plusminusButton, _viewModel.PlusMinusCommand);
            Bind (_percentsButton, _viewModel.PercentsCommand);
            Bind (_divButton, _viewModel.DivCommand);
            Bind (_multButton, _viewModel.MultCommand);
            Bind (_minusButton, _viewModel.MinusCommand);
            Bind (_plusButton, _viewModel.PlusCommand);
            Bind (_equalButton, _viewModel.EqualCommand);
            Bind (_pointButton, _viewModel.PointCommand);
            Bind (_facebookButton, _viewModel.FacebookLoginCommand);
        }
    }
}

